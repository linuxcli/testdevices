#!/bin/bash
# Script Name: testDevices.sh
# Description: Create standard, LVM, RAID partitions of ext4 and XFS filesystem.
# Version: 0.6.4
# Author: Sanju P Debnath
# Date: 01-12-2023
# ChangeLog: Changed wiping partitions.

# For best result mention empty directory to create mount directories for devices.
MOUNT_DIR="/var/data"
DSLIMIT="6"
DEVa='/dev/sdb'
DEVb='/dev/sdc'

function DASH {
CHARCOUNT=$(echo $MSG|wc -c)
echo -n "-"
 for A in $(seq $CHARCOUNT)
        do echo -n  "-"
 done
 }

if [ -z "$(mdadm --help 2> /dev/null)" ]
then 
echo -e "|-----------------------------------------------------------|
| Linux RAID manage tools is not installed on local machine |
| kindly install mdadm packages to use this script by	    |
| by using following command                                |
| dnf install mdadm -yq      Or                             |
| apt-get update && apt-get install mdadm -y                |
|-----------------------------------------------------------|"
	
	exit 1
fi

if [ -z "$(lvcreate --help 2> /dev/null)" ]
then 
echo -e "|----------------------------------------------------|
| LVM manage tools is not installed on local machine |
| kindly install lvm2 packages to use this script by |
| by using following command                         |
| dnf install lvm2 -yq       Or                      |
| apt-get update && apt-get install lvm2 -y          |
|----------------------------------------------------|"
	exit 2
fi

if [ "$USER" = "root" ]
then
input="$(echo "$1" | tr '[:upper:]' '[:lower:]')"


	if [ "${input}" = "create" ]
then
if [ "$2" = 'mbr' ]
then
	TBL='dos'
elif [ "$2" = 'gpt' ]
then
	TBL='gpt'
else
echo "Add second switch for the partition table in small case like mbr or gpt."
exit 1
fi

echo "Calculating require device size"
TDC="$(fdisk -l |awk -F, '/ \/dev\/sd[b-z]/{print $1}'|awk '/GiB/{print $3}'|sort|sed '1p;d')"
RDC="$((($TDC-1)/6))"

if [ "$TDC" -ge "$DSLIMIT" ]
then
for DSK in $(echo ${DEVa} ${DEVb})
do

PARTPROBE () { partprobe $DSK; }

PARTPROBE
#echo "Wiping partition info"
#dd if=/dev/zero of=${DSK} bs=512 count=40960 &> /dev/null


if [ "${TBL}" = 'dos' ]
then
TBL='dos'
echo "Creating $TBL table on disk ${DSK}"
echo "label:${TBL}"|sfdisk ${DSK} &> /dev/null
echo "${DSK}1: id=83, size=${RDC}G
${DSK}2: id=8e, size=${RDC}G
${DSK}3: id=5
${DSK}5: id=fd, size=${RDC}G
${DSK}6: id=fd, size=${RDC}G
${DSK}7: id=fd, size=${RDC}G
${DSK}8: id=fd, size=${RDC}G" | sfdisk ${DSK}
 PARTPROBE

elif [ "${TBL}" = 'gpt' ]
then
TBL='gpt'
SFDVER="$(sfdisk -v|awk '{print $NF}'|awk -F. '{print $1$2}')"
echo "Creating $TBL table on disk ${DSK}"
echo "label:${TBL}"|sfdisk ${DSK} &> /dev/null
if [ "${SFDVER}" -le "233" ]
then
echo "${DSK}1: id=20, size=${RDC}G
${DSK}2: id=31, size=${RDC}G
${DSK}5: id=29, size=${RDC}G
${DSK}6: id=29, size=${RDC}G
${DSK}7: id=29, size=${RDC}G
${DSK}8: id=29, size=${RDC}G" | sfdisk ${DSK}
elif [ "${SFDVER}" -gt "233" ]
then 
echo "${DSK}1: type=L, size=${RDC}G
${DSK}2: type=V, size=${RDC}G
${DSK}5: type=R, size=${RDC}G
${DSK}6: type=R, size=${RDC}G
${DSK}7: type=R, size=${RDC}G
${DSK}8: type=R, size=${RDC}G" | sfdisk ${DSK}
fi
 PARTPROBE

fi

done

5_sec () { for a in {1..5};do echo -en "\rWaiting RAID device to start $a";sleep 1;done;echo ""; }

echo -e "\n --------------------------------------------"
echo " | Preparing filesystem on standard devices |"
echo -e " --------------------------------------------"
DEV="${DEVa}1"
FS="ext4"
DIR_DEV="${MOUNT_DIR}/${FS}"
echo "Creating ${FS} filesystem on standard device ${DEV}"
mkfs.${FS} -q -L ${FS} ${DEV}
echo "Adding mount entries for ${FS} device"
if test -d ${DIR_DEV}
then
echo "Directory ${DIR_DEV} already exist"
else 
mkdir -p ${DIR_DEV} &> /dev/null
fi
echo -e "#Adding partitions of test devices" >> /etc/fstab
DEVID=$(blkid ${DEV}|awk -FUUID= '{print $2}'|awk -F\" '{print $2}')
echo -e "UUID=${DEVID}\t${DIR_DEV}\t${FS}\tdefaults\t0 0" >> /etc/fstab

DEV="${DEVb}1"
FS="xfs"
DIR_DEV="${MOUNT_DIR}/${FS}"
echo "Creating ${FS} filesystem on standard device $DEV"
mkfs.${FS} -q -L ${FS} ${DEV}
echo "Adding mount entries for ${FS} device"
if test -d ${DIR_DEV}
then
echo "Directory ${DIR_DEV} already exist"
else 
mkdir -p ${DIR_DEV} &> /dev/null
fi
DEVID=$(blkid ${DEV}|awk -FUUID= '{print $2}'|awk -F\" '{print $2}')
echo -e "UUID=${DEVID}\t${DIR_DEV}\t${FS}\tdefaults\t0 0" >> /etc/fstab


echo -e "\n ----------------------------------------"
echo " | Preparing filesystem on RAID devices |"
echo -e " ----------------------------------------"

FS="ext4"
DEV="/dev/md/${FS}"
DIR_DEV="${MOUNT_DIR}/${FS}_RAID"
echo "Creating RAID 1 device for ${FS} filesystem"
yes|mdadm -C ${DEV} -qf -l1 -n2 ${DEVa}5 ${DEVb}5 &> /dev/null
5_sec
echo "Creating ${FS} filesystem on RAID device ${DEV}"
mkfs.${FS} -q -L ${FS}-RAID ${DEV}
echo "Adding mount entries for ${FS} device"
if test -d ${DIR_DEV}
then
echo "Directory ${DIR_DEV} already exist"
else 
mkdir -p ${DIR_DEV} &> /dev/null
fi
DEVID=$(blkid ${DEV}|awk -FUUID= '{print $2}'|awk -F\" '{print $2}')
echo -e "UUID=${DEVID}\t${DIR_DEV}\t${FS}\tdefaults\t0 0" >> /etc/fstab

FS="xfs"
DEV="/dev/md/${FS}"
DIR_DEV="${MOUNT_DIR}/${FS}_RAID"
echo "Creating RAID 1 device for ${FS} filesystem"
yes|mdadm -C ${DEV} -qf -l1 -n2 ${DEVa}6 ${DEVb}6 &> /dev/null
5_sec
echo "Creating ${FS} filesystem on RAID device ${DEV}"
mkfs.${FS} -q -L ${FS}-RAID ${DEV}
echo "Adding mount entries for ${FS} device"
if test -d ${DIR_DEV}
then
echo "Directory ${DIR_DEV} already exist"
else 
mkdir -p ${DIR_DEV} &> /dev/null
fi
DEVID=$(blkid ${DEV}|awk -FUUID= '{print $2}'|awk -F\" '{print $2}')
echo -e "UUID=${DEVID}\t${DIR_DEV}\t${FS}\tdefaults\t0 0" >> /etc/fstab


echo -e "\n ----------------------------------------------"
echo -e " | Preparing filesystem on RAID + LVM devices |"
echo -e " ----------------------------------------------"
VG="RAID"
FS="ext4"
DEV="/dev/md/${FS}-${VG}"
LDEV="/dev/${VG}/${FS}"
DIR_DEV="${MOUNT_DIR}/${FS}_${VG}_LVM"
echo "Creating RAID 1 device for ${FS} filesystem"
yes|mdadm -C ${DEV} -qf  -l1 -n2 ${DEVa}7 ${DEVb}7 &> /dev/null
5_sec
echo "Creating LVM device on RAID devices"
pvcreate ${DEV}
vgcreate ${VG} ${DEV} -y -q
lvcreate -n ${FS} -l 100%FREE ${VG} -y -q
echo "Creating ${FS} filesystem on LVM device ${LDEV}"
mkfs.${FS} -q -L ${FS}-${VG}-LVM ${LDEV}
echo "Adding mount entries for ${FS} device"
if test -d ${DIR_DEV}
then
echo "Directory ${DIR_DEV} already exist"
else 
mkdir -p ${DIR_DEV} &> /dev/null
fi
DEVID=$(blkid ${LDEV}|awk -FUUID= '{print $2}'|awk -F\" '{print $2}')
echo -e "UUID=${DEVID}\t${DIR_DEV}\t${FS}\tdefaults\t0 0" >> /etc/fstab

VG="RAID"
FS="xfs"
DEV="/dev/md/${FS}-${VG}"
LDEV="/dev/${VG}/${FS}"
DIR_DEV="${MOUNT_DIR}/${FS}_${VG}_LVM"
echo "Creating RAID 1 device for ${FS} filesystem"
yes|mdadm -C ${DEV} -qf -l1 -n2 ${DEVa}8 ${DEVb}8 &> /dev/null
5_sec
echo "Creating LVM device on RAID devices"
pvcreate ${DEV}
vgextend ${VG} ${DEV} -y -q
lvcreate -n ${FS} -l 100%FREE ${VG} -y -q
echo "Creating ${FS} filesystem on LVM device ${LDEV}"
mkfs.${FS} -q -L ${FS}-${VG}-LVM ${LDEV}
echo "Adding mount entries for ${FS} device"
if test -d ${DIR_DEV}
then
echo "Directory ${DIR_DEV} already exist"
else 
mkdir -p ${DIR_DEV} &> /dev/null
fi
DEVID=$(blkid ${LDEV}|awk -FUUID= '{print $2}'|awk -F\" '{print $2}')
echo -e "UUID=${DEVID}\t${DIR_DEV}\t${FS}\tdefaults\t0 0" >> /etc/fstab


echo -e "\n ---------------------------------------"
echo -e " | Preparing filesystem on LVM devices |"
echo -e " ---------------------------------------"
DEV="${DEVa}2"
FS="ext4"
VG="LVM"
LDEV="/dev/${VG}/${FS}"
DIR_DEV="${MOUNT_DIR}/${FS}_${VG}"
echo "Creating LVM device on standard devices"
pvcreate ${DEV}
vgcreate ${VG} ${DEV} -y -q
lvcreate -n ${FS} -l 100%FREE ${VG} -y -q
echo "Creating ${FS} filesystem on LVM device ${LDEV}"
mkfs.${FS} -q -L ${FS}-${VG} ${LDEV}
echo "Adding mount entries for ${FS} device"
if test -d ${DIR_DEV}
then
echo "Directory ${DIR_DEV} already exist"
else 
mkdir -p ${DIR_DEV} &> /dev/null
fi
DEVID=$(blkid ${LDEV}|awk -FUUID= '{print $2}'|awk -F\" '{print $2}')
echo -e "UUID=${DEVID}\t${DIR_DEV}\t${FS}\tdefaults\t0 0" >> /etc/fstab

DEV="${DEVb}2"
FS="xfs"
VG="LVM"
LDEV="/dev/${VG}/${FS}"
DIR_DEV="${MOUNT_DIR}/${FS}_${VG}"
echo "Creating LVM device on standard devices"
pvcreate ${DEV}
vgextend ${VG} ${DEV} -y -q
lvcreate -n ${FS} -l 100%FREE ${VG} -y -q
echo "Creating ${FS} filesystem on LVM device ${LDEV}"
mkfs.${FS} -q -L ${FS}-${VG} ${LDEV}
echo "Adding mount entries for ${FS} device"
if test -d ${DIR_DEV}
then
echo "Directory ${DIR_DEV} already exist"
else 
mkdir -p ${DIR_DEV} &> /dev/null
fi
DEVID=$(blkid ${LDEV}|awk -FUUID= '{print $2}'|awk -F\" '{print $2}')
echo -e "UUID=${DEVID}\t${DIR_DEV}\t${FS}\tdefaults\t0 0" >> /etc/fstab

echo "Mounting all devices"
mount -av

else
	echo "Require minimum 2 additional disks with 7 GiB each"
	fi


	elif  [ "${input}" = "delete" ]
then

#++++++++++ Clean Devices ++++++++++

echo "Remove device entries from fstab file"
sed -i '/#Adding partitions of test devices/d' /etc/fstab
FS_D="${MOUNT_DIR}/ext"
EXT=$(echo $FS_D|sed "s%\/%\\\/%g")
sed -i "/$EXT/d" /etc/fstab
FS_D="${MOUNT_DIR}/xfs"
XFS=$(echo $FS_D|sed "s%\/%\\\/%g")
sed -i "/$XFS/d" /etc/fstab

echo "Unmounting devices"
umount ${MOUNT_DIR}/ext4* ${MOUNT_DIR}/xfs* &> /dev/null
echo "Removing mount directories"
rm -rf ${MOUNT_DIR}/ext4* ${MOUNT_DIR}/xfs* &> /dev/null

echo "Removing LVM devices"
vgremove 'RAID' -yq
vgremove 'LVM' -yq
pvremove -yq '/dev/md/ext4-RAID'
pvremove -yq '/dev/md/xfs-RAID'
pvremove -yq '/dev/sdb2'
pvremove -yq '/dev/sdc2'

echo "Removing RAID devices"
for a in ${DEVa}{5..8} ${DEVb}{5..8}
do
mdadm --remove $a  &> /dev/null
sleep 1
done

for a in $(ls -1 /dev/md/*)
do
echo "Stopping mdadm device $a"
mdadm --stop $a &> /dev/null
sleep 3
echo "Removing mdadm device $a"
done

echo "Clearing info from partitions"
for a in ${DEVa}{{1,2},{5..8}} ${DEVb}{{1,2},{5..8}}
do
dd if=/dev/zero of=$a bs=512 count=40960 &> /dev/null
done
echo "Clearing parition info from disks"
for a in ${DEVa} ${DEVb}
do
dd if=/dev/zero of=$a bs=512 count=40960 &> /dev/null
done

partprobe -s ${DEVa}
partprobe -s ${DEVb}

	elif [ "${input}" = "" ]
then

MSG=" | This is blank switch, kindly provide create or delete switch | "
echo -e "$(DASH)\n${MSG}\n$(DASH)"

	else
MSG=" | $1 is unknown switch, kindly provide create or delete switch | "
echo -e "$(DASH)\n${MSG}\n$(DASH)"
 	fi

	else
MSG=" | root user privilege require | "
echo -e "$(DASH)\n${MSG}\n$(DASH)"
fi
