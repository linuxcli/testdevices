Version: `0.4` - This is a legacy script which contains deprecated parameters. <br/>
Version: `0.5` - To function this script with Debian 11 OS parameters have been updated.<br/>
<br/>
- This script create total 6 partitions for testing the data on LVM, RAID and RAID + LVM devices formatted with Ext4 and XFS filesystem on it.
- Default mount points are created under /var directory.
- This script writes and remove the entry of the mount point from /etc/fstab file
- For this script 2 devices are require by default it checks /dev/sdb and /dev/sdc
<br/>

Version: `0.6.0`<br/>
ChangeLog 
- [ ] Refer partition by id instead of type due to invalid argument error in sfdisk util-linux package version 2.32.<br/>
(Bug link https://github.com/util-linux/util-linux/issues/1113)
- [ ] Added second switch for the option to add MBR or GPT partition table.<br/><br/>

Version: `0.6.1`<br/>
ChangeLog
- [ ] In Alma Linux 9 if 'id' field is used for GPT partition table it gives 'invalid argument' error message hence changed to 'type' field instead.<br/><br/>

Version: `0.6.2`<br/>
ChangeLog
- [ ] Added condition for field 'type' or 'id' would be use for creating partitions as per the util-linux package version.<br/><br/>

Version: `0.6.3`<br/>
ChangeLog
- [ ] # ChangeLog: Updated how to know sfdisk version.<br/><br/>

Version: `0.6.4`<br/>
ChangeLog
- [ ] #  ChangeLog: Changed wiping partitions.<br/><br/>


Note: For the changes done in the script kindly read ChangeLog topic in the script.
